/**
 * Created by denistrofimov on 30.12.15.
 */

var http = require('http');
var express = require('express');
var bodyParser = require('body-parser');
var convert = require('./convert');
var json = require('./json');
var path = require('path');
var sendmail = require('sendmail')();
var exec = require('sync-exec');
var handlebars = require('handlebars');
var template = handlebars.compile('' +
    '{{#if stdout}}<h1>Output</h1>' +
    '<pre style="background-color: #383838;color:white;padding:20px;border-radius: 10px">{{stdout}}</pre>{{/if}}' +
    '{{#if stderr}}<h1>Errors</h1>' +
    '<pre style="background-color: #383838;color:red;padding:20px;border-radius: 10px">{{stderr}}</pre>{{/if}}' +
    '<h1>Status code: {{status}}</h1>');

module.exports = function (port, configPath) {
    var app = express();
    app.use(bodyParser.json());
    app.all('/push', function (req, res, next) {

        var config = json.file(configPath);

        var repositories = config['repositories'];
        var remote = convert(req.body);
        var branch = convert.branch(req.body) || "master";

        if (!remote)
            return next(new Error('Undefined repository'));

        if (!repositories[remote])
            return next(new Error('Repository remote ' + remote + ' unspecified'));

        res.send({
            result: "ok"
        });

        var local = repositories[remote];
        var result = exec('bash ' + path.join(local, 'deploy.sh ' + branch), {cwd: local});

        var email = config['callback_email'];

        if (email)
            sendmail({
                from: email,
                to: email,
                subject: 'Webhook for ' + remote,
                type: 'text/html',
                content: template(result)
            });
    });
    app.use(function (error, req, res, next) {
        console.error(error);
        res.status(500);
        res.send({
            message: error.message
        });
    });

    http.createServer(app).listen(port);
};