/**
 * Created by denistrofimov on 30.12.15.
 */

var json = require('./json');
var path = require('path');
var argv = require('yargs')['argv'];
var fs = require('fs');
var server = require('./server');

module.exports = function (configPath) {
    return {
        repository: {
            add: function () {
                var config = json.file(configPath);
                config.repositories[argv._[2]] = process.cwd();
                fs.writeFileSync(configPath, JSON.stringify(config));
                console.log('repository ' + argv._[2] + ' added as ' + process.cwd());
            },
            remove: function () {
                var config = json.file(configPath);
                delete config.repositories[argv._[0]];
                fs.writeFileSync(configPath, JSON.stringify(config));
                console.log('repository ' + argv._[2] + ' has been removed');
            }
        },
        server: {
            start: function () {
                var port = argv.port || json.file(configPath)['port'] || 8082;
                server(port, configPath);
                console.log('Webhooker server started on port ' + port);
            }
        },
        mail: {
            setup: function () {
                var config = json.file(configPath);
                config.callback_email = argv._[2];
                fs.writeFileSync(configPath, JSON.stringify(config));
            },
            clear: function () {
                var config = json.file(configPath);
                config.callback_email = null;
                fs.writeFileSync(configPath, JSON.stringify(config));
            }
        }
    };
};