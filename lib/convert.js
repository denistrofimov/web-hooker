/**
 * Created by denistrofimov on 30.12.15.
 */

module.exports = function (data) {
    if (data['repository'] && data['repository']['full_name'])
        return data['repository']['full_name'];
    return null;
};

module.exports.branch = function (data) {
    if (data['ref']) {
        var parts = data['ref'].split('/');
        return parts[parts.length - 1];
    }
    if (data['push'] && data['push']['changes'] && data['push']['changes'].length && data['push']['changes'][0]['new'])
        return data['push']['changes'][0]['new']['name'];
};