/**
 * Created by denistrofimov on 30.12.15.
 */

var fs = require('fs');

var json = module.exports = function (input) {
    try {
        return JSON.parse(input);
    } catch (e) {
        return null;
    }
};

json.file = function (file) {
    return json(fs.readFileSync(file));
};