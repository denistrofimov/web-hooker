[![NPM](https://nodei.co/npm/node-webhooker.png?downloads=true&downloadRank=true&stars=true)](https://www.npmjs.com/package/node-webhooker)

#Install

`npm install node-webhooker -g`

#Commands

`webhooker repository add [repository remote full name]` - add cwd for listening on webhooks from **[repository remote full name]**

`webhooker repository remove [repository remote full name]` - remove **[repository remote full name]** from listening for webhooks

`webhooker server start --port [port]` - start webhooker server on **[port]** (default is 8082)

#Usage

- Place `deploy.sh` with deployment script in project root directory. Script will be run with *branch* as first parameter.
- Run `webhooker repository add [repository remote full name]` in project root directory
- Start server with `webhooker server start --port [port]` command
- Set `http(s)://[host/ip]:[port]/push` as webhook url