#!/usr/bin/env node


/**
 * Created by Denis Trofimov on 15.05.15.
 */
var path = require('path');
var json = require('../lib/json');
var homedir = require('os-homedir');
var tildify = require('tildify');
var mkdir = require('mkdirp');
var fs = require('fs');
var argv = require('yargs')['argv'];

var module = argv._.shift();
var command = argv._.shift();

var defaultConfig = {
    repositories: {}
};

var home = homedir();
var webhookerDir = path.join(home, '.webhooker');
var repositoriesPath = path.join(home, '.webhooker', 'config.json');

if (!fs.existsSync(webhookerDir))
    mkdir.sync(path.join(home, '.webhooker'));

if (!fs.existsSync(repositoriesPath))
    fs.writeFileSync(repositoriesPath, JSON.stringify(defaultConfig));

console.log('Use ' + tildify(repositoriesPath) + '');

var cli = require('../lib/cli')(repositoriesPath);

if (cli[module] && cli[module][command])
    cli[module][command]();